package cToF;

public class CToF {

    public double getFarenheit(String celsius) {
        return Double.parseDouble(celsius) * 1.8 + 32;
    }

}
