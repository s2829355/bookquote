package cToF;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DegreeChange extends HttpServlet {
    CToF cel;

    public DegreeChange() {
        cel = new CToF();
    }

    public void doGet(HttpServletRequest request,
                      HttpServletResponse response)
            throws ServletException, IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();


        out.println("<!DOCTYPE HTML>\n" +
                "<HTML>\n" +
                "<HEAD><TITLE> Celsius to Farenheit </TITLE>" +
                "</HEAD>\n" +
                "<BODY\n" +
                "<H1> Celsius to Farenheit </H1>\n" +
                "  <P> Celsius " +
                request.getParameter("degree") + "\n" +
                "  <P> Farenheit " +
                Double.toString(cel.getFarenheit(request.getParameter("degree"))) +
                "</BODY></HTML>");
    }


}

