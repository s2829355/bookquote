
import cToF.CToF;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class TestDegreeChange {


    @Test
    public void testCToF(){
        CToF cel = new CToF();
        assertEquals(14,cel.getFarenheit("-10") );
        assertEquals(-58, cel.getFarenheit("-50"));
        assertEquals(32, cel.getFarenheit("0"));
    }
}
